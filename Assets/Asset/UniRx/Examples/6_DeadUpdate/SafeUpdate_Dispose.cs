﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;
public class SafeUpdate_Dispose : Base
{

	// Use this for initialization
	void Start () {

		gameObject.transform.position = new Vector2(0, 1f);

		IDisposable mover = Observable.Interval(TimeSpan.FromMilliseconds(500))
			.TakeUntilDestroy(this)
			.Subscribe(l => Move(0.1f, 0));

		//5秒後にgameObjectが死ぬ
		Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(_ => 
			{
				mover.Dispose();//moverを破棄して購読停止
				Destroy(gameObject);
			});
	}
}