﻿using UnityEngine;
using System.Collections;
using UniRx;
using UniRx.Triggers;

public class WhereSample : Base {

	// Use this for initialization
	void Start () {
		//Returnで(0,0.5)という値をSubscribe内に流し込んでる
		Observable.Return(new Vector2(0, 0.5f)).Subscribe(v => gameObject.transform.position = v);

		//Whereで左クリックの間しか値がプッシュされないようにしている
		this.UpdateAsObservable()
			.Where(_ => Input.GetMouseButton(0))
			.Subscribe(l => Move(0.01f, 0));
	}

}
//
/*
とてもわかりやすいサンプルをありがとうございます。大変勉強になります。
ところですみません　質問です
Unity5.3.1 MacOS10.11.1です
このサンプルコードを再生し終わり停止を押した場合に
で「InvalidOperationException: sequence is empty」というエラーが出るのですが。。購読の停止をしてないせいですね

InvalidOperationException: sequence is empty
UniRx.Stubs.<Throw>m__6C (System.Exception ex) (at Assets/Asset/UniRx/Scripts/Observer.cs:257)
UniRx.Observer+Subscribe`1[UniRx.Unit].OnError (System.Exception error) (at Assets/Asset/UniRx/Scripts/Observer.cs:142)
UniRx.Operators.FirstObservable`1+First_[UniRx.Unit].OnCompleted () (at Assets/Asset/UniRx/Scripts/Operators/First.cs:154)
UniRx.Subject`1[UniRx.Unit].OnCompleted () (at Assets/Asset/UniRx/Scripts/Subjects/Subject.cs:38)
UniRx.Triggers.ObservableUpdateTrigger.RaiseOnCompletedOnDestroy () (at Assets/Asset/UniRx/Scripts/UnityEngineBridge/Triggers/ObservableUpdateTrigger.cs:27)
UniRx.Triggers.ObservableTriggerBase.OnDestroy () (at Assets/Asset/UniRx/Scripts/UnityEngineBridge/Triggers/ObservableTriggerBase.cs:52)


*/
